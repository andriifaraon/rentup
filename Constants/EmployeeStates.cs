﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RentUpWeb.Constants
{
    public static class EmployeeStates
    {
        public const int WORKING = 1;
        public const int RETIRING = 2;
        public const int FIRIRNG = 3;
        public const int TO_BE_FIRED = 4;
    }
}
