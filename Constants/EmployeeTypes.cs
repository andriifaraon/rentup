﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RentUpWeb.Constants
{
    public class EmployeeTypes
    {
        public const int AGENT = 1;
        public const int MANAGER = 2;
        public const int ADMIN = 3;
    }
}
