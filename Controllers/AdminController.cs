﻿using DataLayer.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RentUpWeb.ViewModels;
using RentUpWeb.Constants;

namespace RentUpWeb.Controllers
{
    [Route("[controller]/[action]")]
    public class AdminController : Controller
    {
        private readonly ApplicationDbContext _context;
        public AdminController(ApplicationDbContext context) => _context = context;

        [HttpGet]
        [Route("~/Admin/AdminMainMenu")]
        public IActionResult AdminMainMenu()
        {
            return View();
        }

        [HttpGet]
        [Route("~/Admin/Statistics")]
        public IActionResult Statistics()
        {
            DateTime dateForStatistic = DateTime.Now.AddMonths(-1);
            Statistics statistics = new Statistics();

            statistics.Payments = _context.Payment.Where(p => p.PaymentDate >= dateForStatistic).ToList();
            statistics.Contracts = _context.Contract.Where(c => c.SignedDate >= dateForStatistic)
                                                    .Select(c => new ContractToList()
                                                    {
                                                        ContractID = c.ContractID,
                                                        ClientEmail = c.ClientEmail,
                                                        OwnerEmail = c.Space.OwnerEmail
                                                    }).ToList();
            statistics.TotalIncome = _context.Contract.Select(c => c.OwnerPayment).Sum();
            statistics.TotalOutcome = _context.Payment.Where(p => p.ContractID == null)
                                                      .Select(p => p.PaymentSum).Sum();
            statistics.CompanyBalance = _context.Employee.FirstOrDefault(e => e.EmployeeTypeID == EmployeeTypes.ADMIN)
                                                         .EmployeeSum!;
            statistics.Profit = statistics.TotalIncome - statistics.TotalOutcome;
            statistics.WorkingEmployees = _context.Employee.Where(e => e.EmployeeStateID == EmployeeStates.WORKING).Count();
            statistics.RetiredEmployees = _context.Employee.Where(e => e.EmployeeStateID == EmployeeStates.RETIRING).Count();
            statistics.ToBeFiredEmployees = _context.Employee.Where(e => e.EmployeeStateID == EmployeeStates.TO_BE_FIRED).Count();
            statistics.FiredEmployees = _context.Employee.Where(e => e.EmployeeStateID == EmployeeStates.FIRIRNG).Count();
            statistics.SpacesCount = _context.Space.Count();
            statistics.OwnersCount = _context.Owner.Count();
            statistics.ClientsCount = _context.Client.Count();
            statistics.CitiesCount = _context.City.Count();

            return View(statistics);
        }
    }
}
