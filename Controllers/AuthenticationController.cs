﻿using Microsoft.AspNetCore.Mvc;
using DataLayer.Models;
using RentUpWeb.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RentUpWeb.Constants;

namespace RentUpWeb.Controllers
{
    [Route("[controller]/[action]")]
    public class AuthenticationController : Controller
    {
        private readonly ApplicationDbContext _context;
        public AuthenticationController(ApplicationDbContext context) => _context = context;

        [HttpGet]
        [Route("~/Authentication/SignIn")]
        public IActionResult SignIn()
        {
            return View();
        }

        [HttpPost]
        [Route("~/Authentication/SignIn")]
        public IActionResult SignIn(EmployeeSignIn employeeSignIn)
        {
            if(ModelState.IsValid)
            {
                var employee = _context.Employee.FirstOrDefault(e => e.EmployeeEmail == employeeSignIn.EmployeeEmail);

                if (employee != null)
                {
                    if (employee.EmployeeStateID != EmployeeStates.WORKING && employee.EmployeeStateID != EmployeeStates.TO_BE_FIRED)
                    {
                        ModelState.AddModelError("EmployeeEmail", "You don`t work here anymore");
                        return View(employeeSignIn);
                    }
                    if (employee.EmployeePassword == employeeSignIn.EmployeePassword)
                    {
                        TempData["RegisteredEmployee"] = employee.EmployeeEmail;
                        TempData["EmployeeState"] = employee.EmployeeStateID;
                        TempData["DepartmentID"] = employee.DepartmentID;

                        if (employee.EmployeeTypeID == EmployeeTypes.AGENT)
                            return RedirectToAction("AgentMainMenu", "Agent");
                        else if (employee.EmployeeTypeID == EmployeeTypes.MANAGER)
                            return RedirectToAction("ManagerMainMenu", "Manager");
                        else if (employee.EmployeeTypeID == EmployeeTypes.ADMIN)
                            return RedirectToAction("AdminMainMenu", "Admin");
                    }
                    else
                    {
                        ModelState.AddModelError("EmployeePassword", "Password is incorrect");
                        return View(employeeSignIn);
                    }          
                }

                ModelState.AddModelError("EmployeeEmail", "Such employee isn`t exist");
                return View(employeeSignIn);
            }
            return View();
        }
    }
}
