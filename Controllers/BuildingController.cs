﻿using DataLayer.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using RentUpWeb.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RentUpWeb.Controllers
{
    public class BuildingController : Controller
    {
        private readonly ApplicationDbContext _context;
        public BuildingController(ApplicationDbContext context) => _context = context;


        [HttpGet]
        [Route("~/Building/Buildings")]
        public IActionResult Buildings()
        {
            return View(_context.Building.Select
                                            (b => new BuildingToList()
                                            {
                                                City = b.City.CityName,
                                                BuildingStreet = b.BuildingStreet,
                                                BuildingNumber = b.BuildingNumber
                                            }
                                            )
                                            .ToList());
        }

        [HttpGet]
        [Route("~/Building/AddBuilding")]
        public IActionResult AddBuilding()
        {
            ViewBag.Cities = new SelectList(_context.City, "CityID", "CityName");
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> AddBuilding(BuildingToAdd buildingToAdd)
        {
            if (ModelState.IsValid)
            {
                var building = new Building()
                {
                    BuildingNumber = buildingToAdd.BuildingNumber,
                    BuildingStreet = buildingToAdd.BuildingStreet,
                    CityID = buildingToAdd.CityID,
                    CreateDate = DateTime.Now,
                    UpdateDate = DateTime.Now
                };

                await _context.Building.AddAsync(building);
                await _context.SaveChangesAsync();

                return RedirectToAction("Buildings", "Building");
            }

            ViewBag.Cities = new SelectList(_context.City, "CityID", "CityName");
            return View();
        }
    }
}
