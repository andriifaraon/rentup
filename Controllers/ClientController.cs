﻿using DataLayer.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RentUpWeb.ViewModels;
using Microsoft.AspNetCore.Mvc.Rendering;
using RentUpWeb.Util;

namespace RentUpWeb.Controllers
{
    [Route("[controller]/[action]")]
    public class ClientController : Controller
    {
        private readonly ApplicationDbContext _context;
        public ClientController(ApplicationDbContext context) => _context = context;


        [HttpGet]
        [Route("~/Client/Clients")]
        public IActionResult Clients(string searchString)
        {
            var clients = _context.Client.Select
                                            (c => new ClientToList()
                                            {
                                                ClientEmail = c.ClientEmail,
                                                ClientNameSurname = c.ClientName + " " + c.ClientSurname,
                                                ClientPhone = c.ClientPhone,
                                                CityName = c.City.CityName,
                                                CanBeDeleted = c.Contracts.Count == 0
                                            }
                                            )
                                            .ToList();

            if (string.IsNullOrEmpty(searchString))
                return View(clients);
            else
                return View(clients.Where(c => SearchStringUtil.Match(searchString, c.ClientEmail, c.ClientNameSurname, c.ClientPhone, c.CityName)).ToList());
        }

        [Route("~/Client/RemoveClient")]
        public async Task<IActionResult> RemoveClient(string email)
        {
            var client = _context.Client.FirstOrDefault(c => c.ClientEmail == email);

            if(client != null)
            {
                _context.Client.Remove(client);
                await _context.SaveChangesAsync();

                return RedirectToAction("Clients", "Client");
            }

            return BadRequest();
        }

        [HttpGet]
        [Route("~/Client/EditClient")]
        public IActionResult EditClient(string email)
        {
            var client = _context.Client.FirstOrDefault(c => c.ClientEmail == email);
            var clientToEdit = new ClientToEdit()
            {
                ClientEmail = client.ClientEmail,
                ClientName = client.ClientName,
                ClientSurname = client.ClientSurname,
                ClientPhone = client.ClientPhone
            };
            return View(clientToEdit);
        }

        [HttpPost]
        [Route("~/Client/EditClient")]
        public async Task<IActionResult> EditClient(ClientToEdit clientToEdit)
        {
            var client = _context.Client.FirstOrDefault(c => c.ClientEmail == clientToEdit.ClientEmail);

            if (client != null)
            {
                client.ClientName = clientToEdit.ClientName;
                client.ClientSurname = clientToEdit.ClientSurname;
                client.ClientPhone = clientToEdit.ClientPhone;

                _context.Client.Update(client);
                await _context.SaveChangesAsync();

                return RedirectToAction("Clients", "Client");
            }

            return BadRequest();
        }

        [HttpGet]
        [Route("~/Client/AddClient")]
        public IActionResult AddClient()
        {
            ViewBag.Cities = new SelectList(_context.City, "CityID", "CityName");
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> AddClient(ClientToAdd clientToAdd)
        {
            if(ModelState.IsValid)
            {
                if (_context.Client.Select(e => e.ClientEmail).Contains(clientToAdd.ClientEmail))
                {
                    ModelState.AddModelError("", "Client with such email already exists");
                    return View();
                }

                var client = new Client()
                {
                    ClientEmail = clientToAdd.ClientEmail,
                    ClientName = clientToAdd.ClientName,
                    ClientSurname = clientToAdd.ClientSurname,
                    ClientPhone = clientToAdd.ClientPhone,
                    CityID = clientToAdd.CityID,
                    CreateDate = DateTime.Now,
                    UpdateDate = DateTime.Now
                };

                await _context.Client.AddAsync(client);
                await _context.SaveChangesAsync();

                return RedirectToAction("Clients", "Client");
            }

            ViewBag.Cities = new SelectList(_context.City, "CityID", "CityName");
            return View();
        }
    }
}
