﻿using DataLayer.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using RentUpWeb.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RentUpWeb.Constants;

namespace RentUpWeb.Controllers
{
    [Route("[controller]/[action]")]
    public class ContractController : Controller
    {
        private readonly ApplicationDbContext _context;
        public ContractController(ApplicationDbContext context) => _context = context;

        [HttpGet]
        [Route("~/Contract/CreateContract")]
        public IActionResult CreateContract()
        {
            ViewBag.Clients = new SelectList(_context.Client, "ClientEmail", "ClientEmail");
            ViewBag.Spaces = new SelectList(_context.Space, "SpaceID", "SpaceName");
            return View();
        }

        [HttpPost]
        public IActionResult CreateContract(ContractToCreate contractToCreate)
        {
            var startDate = contractToCreate.StartDate;
            var endDate = contractToCreate.EndDate;
            var clientEmail = contractToCreate.ClientEmail;
            var employeeEmail = contractToCreate.EmployeeEmail;
            var spaceID = contractToCreate.SpaceID;

            if(endDate <= startDate || endDate <= DateTime.Now || startDate < DateTime.Now)
            {
                ViewBag.Clients = new SelectList(_context.Client, "ClientEmail", "ClientEmail");
                ViewBag.Spaces = new SelectList(_context.Space, "SpaceID", "SpaceName");
                ModelState.AddModelError("", "Wrong dates range");
                return View();
            }
                
            var space = _context.Space.FirstOrDefault(s => s.SpaceID == contractToCreate.SpaceID);
            if(space != null)
            {
                var pricePerDay = space.Price / space.RentalDays;
                var duration = (endDate - startDate).Days;
                var contractSum = pricePerDay * duration;

                var taxPercent = space.Owner.OwnerType.Tax.TaxPercent;
                var taxSum = (contractSum * taxPercent) / 100;

                var ownerPaymentPercent = space.Owner.PaymentPercent;
                var ownerPayment = (contractSum * ownerPaymentPercent) / 100;

                var contract = new Contract()
                {
                    SpaceID = spaceID,
                    ClientEmail = clientEmail,
                    EmployeeEmail = employeeEmail,
                    StartDate = startDate,
                    EndDate = endDate,
                    ContractSum = contractSum,
                    OwnerPayment = ownerPayment,
                    TaxSum = taxSum,
                    ContractStateID = 1,
                    SignedDate = DateTime.Now,
                    CreateDate = DateTime.Now,
                    UpdateDate = DateTime.Now
                };

                var contractsForSpace = _context.Contract.Where(c => c.SpaceID == contract.SpaceID);
                foreach (var item in contractsForSpace)
                {
                    if (endDate <= item.StartDate || startDate >= item.EndDate)
                        continue;
                    else
                    {
                        ViewBag.Clients = new SelectList(_context.Client, "ClientEmail", "ClientEmail");
                        ViewBag.Spaces = new SelectList(_context.Space, "SpaceID", "SpaceName");
                        ModelState.AddModelError("", "This space is already rented fot such dates");
                        return View();
                    }
                }

                return View("CreateContractConfirmation", contract);  
            }

            return BadRequest();  
        }

        [HttpPost]
        public async Task<IActionResult> ConfirmContract(Contract contract)
        {
            var payment = new Payment();
            var employee = _context.Employee.FirstOrDefault(e => e.EmployeeEmail == contract.EmployeeEmail);
            payment.PaymentSum = (employee.EmployeePercent * contract.ContractSum) / 100;
            payment.PaymentDate = DateTime.Now;
            payment.EmployeeEmail = contract.EmployeeEmail;
            

            _context.Contract.Add(contract);
            await _context.SaveChangesAsync();

            payment.ContractID = _context.Contract.OrderByDescending(c => c.ContractID).ToList()[0].ContractID;
            payment.CreateDate = DateTime.Now;
            payment.UpdateDate = DateTime.Now;

            employee.EmployeeSum += payment.PaymentSum;
            employee.EmployeeContractCount += 1;
            _context.Employee.Update(employee);

            var admin = _context.Employee.FirstOrDefault(e => e.EmployeeTypeID == EmployeeTypes.ADMIN);
            if(admin != null)
            {
                admin.EmployeeSum += contract.OwnerPayment;
                _context.Employee.Update(admin);
            }

            _context.Payment.Add(payment);
            await _context.SaveChangesAsync();

            return RedirectToAction("AgentContracts", "Contract", new { email = contract.EmployeeEmail });
        }



        [HttpGet]
        [Route("~/Contract/AgentContracts/{email}")]
        public async Task<IActionResult> AgentContracts(string email)
        {
            var employee = await _context.Employee.FirstOrDefaultAsync(e => e.EmployeeEmail == email);

            if(employee != null)
            {
                IEnumerable<ContractToList> contracts = 
                    employee.Contracts
                            .Select(c => new ContractToList()
                            { 
                                 ContractID = c.ContractID,
                                 OwnerEmail = c.Space.OwnerEmail,
                                 ClientEmail = c.ClientEmail
                            });

                return View(contracts);
            }

            return BadRequest();
        }

        [HttpGet]
        [Route("~/Contract/Details/{id}")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return NotFound();

            var contract = await _context.Contract.FirstOrDefaultAsync(c => c.ContractID == id);

            if (contract == null)
                return NotFound();

            return View(
                new ContractToDetails()
                {
                    ContractID = contract.ContractID,
                    ContractSum = contract.ContractSum,
                    SignedDate = contract.SignedDate,
                    Period = contract.StartDate.ToShortDateString() + "  -  " + contract.EndDate.ToShortDateString(),
                    TaxSum = contract.TaxSum,
                    ContractState = contract.ContractState.ContractStateName,
                    OwnerEmail = contract.Space.OwnerEmail,
                    ClientEmail = contract.ClientEmail,
                    SpaceName = contract.Space.SpaceName,
                    OwnerPayment = contract.OwnerPayment,
                    SpaceTypeName = contract.Space.SpaceType.SpaceTypeName
                });
        }
    }
}
