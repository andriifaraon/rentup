﻿using DataLayer.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using RentUpWeb.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using RentUpWeb.Constants;
using RentUpWeb.Util;

namespace RentUpWeb.Controllers
{
    [Route("[controller]/[action]")]
    public class EmployeeController : Controller
    {
        private readonly ApplicationDbContext _context;
        public EmployeeController(ApplicationDbContext context) => _context = context;

        [HttpGet]
        [Route("~/Employee/HireEmployee")]
        public IActionResult HireEmployee()
        {
            return View();
        }

        [HttpGet]
        [Route("~/Employee/EditEmployee")]
        public IActionResult EditEmployee(string email)
        {
            var employee = _context.Employee.FirstOrDefault(e => e.EmployeeEmail == email);
            var employeeToEdit = new EmployeeToEdit()
            {
                EmployeeEmail = employee.EmployeeEmail,
                EmployeeName = employee.EmployeeName,
                EmployeeSurname = employee.EmployeeSurname
            };
            return View(employeeToEdit);
        }

        [HttpPost]
        [Route("~/Employee/EditEmployee")]
        public async Task<IActionResult> EditEmployee(EmployeeToEdit employeeToEdit)
        {
            var employee = _context.Employee.FirstOrDefault(e => e.EmployeeEmail == employeeToEdit.EmployeeEmail);

            if (employee != null)
            {
                employee.EmployeeName = employeeToEdit.EmployeeName;
                employee.EmployeeSurname = employeeToEdit.EmployeeSurname;

                _context.Employee.Update(employee);
                await _context.SaveChangesAsync();

                return RedirectToAction("DepartmentEmployees", "Employee", new { id = employee.DepartmentID });
            }

            return BadRequest();
        }

        [HttpPost]
        public async Task<IActionResult> HireEmployee(EmployeeToHire employeeToHire)
        {
            if(ModelState.IsValid)
            {
                if(_context.Employee.Select(e => e.EmployeeEmail).Contains(employeeToHire.EmployeeEmail))
                {
                    ModelState.AddModelError("", "Employee with such email already exists");
                    return View();
                }
                else
                {
                    var departmentID = (int)TempData["DepartmentID"];
                    TempData.Keep();

                    var employee = new Employee()
                    {
                        EmployeeEmail = employeeToHire.EmployeeEmail,
                        EmployeePassword = employeeToHire.Password,
                        EmployeeName = employeeToHire.EmployeeName,
                        EmployeeSurname = employeeToHire.EmployeeSurname,
                        EmployeePercent = employeeToHire.EmployeePercent,
                        EmployeeSum = 0,
                        DepartmentID = departmentID, 
                        EmployeeStateID = EmployeeStates.WORKING,
                        EmployeeTypeID = EmployeeTypes.AGENT, 
                        EmployeeContractCount = 0,
                        CreateDate = DateTime.Now,
                        UpdateDate = DateTime.Now
                    };

                    _context.Employee.Add(employee);
                    await _context.SaveChangesAsync();

                    return RedirectToAction("DepartmentEmployees", new { id = departmentID });
                }
            }
            else
            {
                return View();
            }
        }

        [Route("~/Employee/FireEmployee/{email}")]
        public async Task<IActionResult> FireEmployee(string email)
        {
            var employee = _context.Employee.FirstOrDefault(e => e.EmployeeEmail == email);

            if(employee != null)
            {
                var departmentID = (int)TempData["DepartmentID"];
                TempData.Keep();

                var contracts = employee.Contracts;
                bool isActive = contracts.Any(c => c.EndDate >= DateTime.Now);

                if (isActive)
                    employee.EmployeeStateID = EmployeeStates.TO_BE_FIRED;
                else
                    employee.EmployeeStateID = EmployeeStates.FIRIRNG;

                employee.UpdateDate = DateTime.Now;

                _context.Employee.Update(employee);
                await _context.SaveChangesAsync();

                return RedirectToAction("DepartmentEmployees", new { id = departmentID });
            }

            return BadRequest();
        }

        [HttpGet]
        [Route("~/Employee/AllEmployees")]
        public IActionResult AllEmployees(string searchString)
        {
            var employees =
                    _context.Employee.Where(e => e.EmployeeTypeID != EmployeeTypes.ADMIN)
                            .Select(e => new EmployeeToList()
                            {
                                EmployeeEmail = e.EmployeeEmail,
                                EmployeeNameSurname = e.EmployeeName + " " + e.EmployeeSurname,
                                EmployeePositionName = e.EmployeeType.EmployeeTypeName,
                                EmployeeStateName = e.EmployeeState.EmployeeStateName,
                                EmployeeStateID = e.EmployeeStateID,
                                EmployeeTypeID = e.EmployeeTypeID
                            }).ToList();

            if(string.IsNullOrEmpty(searchString))
                return View(employees);
            else
                return View(employees.Where(e => 
                SearchStringUtil.Match(searchString, e.EmployeeEmail, e.EmployeeNameSurname, e.EmployeePositionName, e.EmployeeStateName)));
        }

        
        [Route("~/Employee/ChangeEmployeeType/{email}")]
        public async Task<IActionResult> ChangeEmployeeType(string email)
        {
            if(string.IsNullOrEmpty(email))
            {
                return BadRequest();
            }
            var employee = await _context.Employee.FirstOrDefaultAsync(e => e.EmployeeEmail == email);

            if(employee != null)
            {
                if(employee.EmployeeTypeID == EmployeeTypes.AGENT)
                {
                    employee.EmployeeTypeID = EmployeeTypes.MANAGER;
                }
                else
                {
                    employee.EmployeeTypeID = EmployeeTypes.AGENT;
                }

                _context.Employee.Update(employee);
                await _context.SaveChangesAsync();

                return RedirectToAction("AllEmployees", "Employee");
            }

            return BadRequest();
        }

        [HttpGet]
        [Route("~/Employee/DepartmentEmployees/{id}")]
        public async Task<IActionResult> DepartmentEmployees(int id)
        {
            var department = await _context.Department.FirstOrDefaultAsync(d => d.DepartmentID == id);

            if (department != null)
            {
                IEnumerable<EmployeeToList> employees =
                    department.Employees
                            .Select(e => new EmployeeToList()
                            {
                                EmployeeEmail = e.EmployeeEmail,
                                EmployeeNameSurname = e.EmployeeName + " " + e.EmployeeSurname,
                                EmployeePositionName = e.EmployeeType.EmployeeTypeName,
                                EmployeeStateName = e.EmployeeState.EmployeeStateName,
                                EmployeeStateID = e.EmployeeStateID,
                                EmployeeTypeID = e.EmployeeTypeID
                            });

                return View(employees);
            }

            return BadRequest();
        }

        [Route("~/Employee/PaySalary/{email}")]
        public async Task<IActionResult> PaySalary(string email)
        {
            var employee = _context.Employee.FirstOrDefault(e => e.EmployeeEmail == email);
            var admin = _context.Employee.FirstOrDefault(e => e.EmployeeTypeID == EmployeeTypes.ADMIN);

            if (employee != null && admin != null)
            {
                var baseSalary = employee.EmployeeType.BaseSalary;
                if(baseSalary <= admin.EmployeeSum)
                {
                    employee.EmployeeSum += baseSalary;
                    admin.EmployeeSum -= baseSalary;

                    employee.UpdateDate = DateTime.Now;
                    admin.UpdateDate = DateTime.Now;

                    _context.Employee.Update(employee);
                    _context.Employee.Update(admin);

                    var payment = new Payment()
                    {
                        PaymentSum = baseSalary,
                        PaymentDate = DateTime.Now,
                        EmployeeEmail = email,
                        CreateDate = DateTime.Now,
                        UpdateDate = DateTime.Now
                    };
                    _context.Payment.Add(payment);

                    await _context.SaveChangesAsync();

                    return RedirectToAction("DepartmentEmployees", new { id = employee.DepartmentID });
                }
                else
                {
                    ModelState.AddModelError("", "There isn`t enough money to pay salary");
                    return RedirectToAction("DepartmentEmployees", new { id = employee.DepartmentID });
                }
            }

            return BadRequest();
        }
    }
}
