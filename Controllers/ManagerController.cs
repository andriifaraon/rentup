﻿using DataLayer.Models;
using Microsoft.AspNetCore.Mvc;
using RentUpWeb.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RentUpWeb.Controllers
{
    [Route("[controller]/[action]")]
    public class ManagerController : Controller
    {
        private readonly ApplicationDbContext _context;
        public ManagerController(ApplicationDbContext context) => _context = context;

        [HttpGet]
        [Route("~/Manager/ManagerMainMenu")]
        public IActionResult ManagerMainMenu()
        {
            return View();
        }

        [HttpGet]
        [Route("~/Manager/Rate/{email}")]
        public IActionResult Rate(string email)
        {
            var employee = _context.Employee.FirstOrDefault(e => e.EmployeeEmail == email);
            if (employee != null)
            {
                var employeeRate = new EmployeeRate()
                {
                    EmployeeEmail = employee.EmployeeEmail,
                    EmployeeName = employee.EmployeeName,
                    EmployeeSurname = employee.EmployeeSurname,
                    EmployeeTypeName = employee.EmployeeType.EmployeeTypeName,
                    DepartmentEmail = employee.Department.DepartmentEmail,
                    ContractPercent = employee.EmployeePercent,
                    ContractSum = employee.EmployeeSum,
                    ContractNumber = employee.EmployeeContractCount
                };

                return View("ManagerRate", employeeRate);
            }

            return BadRequest();
        }
    }
}
