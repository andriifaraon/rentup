﻿using DataLayer.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using RentUpWeb.Util;
using RentUpWeb.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RentUpWeb.Controllers
{
    public class OwnerController : Controller
    {
        private readonly ApplicationDbContext _context;
        public OwnerController(ApplicationDbContext context) => _context = context;


        [HttpGet]
        [Route("~/Owner/Owners")]
        public IActionResult Owners(string searchString)
        {
            var owners = _context.Owner.Select
                                            (o => new OwnerToList()
                                            {
                                                OwnerEmail = o.OwnerEmail,
                                                OwnerNameSurname = o.OwnerName + " " + o.OwnerSurname,
                                                OwnerPhone = o.OwnerPhone,
                                                City = o.City.CityName,
                                                OwnerType = o.OwnerType.OwnerTypeName,
                                                PaymentPercent = o.PaymentPercent,
                                                CanBeDeleted = o.Spaces.Count == 0
                                            }
                                            )
                                            .ToList();

            if (string.IsNullOrEmpty(searchString))
                return View(owners);
            else
                return View(owners.Where(o => SearchStringUtil.Match
                (searchString, o.OwnerEmail, o.OwnerNameSurname, o.OwnerPhone, o.City, o.PaymentPercent.ToString(), o.OwnerType)).ToList());
        }

        [Route("~/Owner/RemoveOwner")]
        public async Task<IActionResult> RemoveOwner(string email)
        {
            var owner = _context.Owner.FirstOrDefault(o => o.OwnerEmail == email);

            if (owner != null)
            {
                _context.Owner.Remove(owner);
                await _context.SaveChangesAsync();

                return RedirectToAction("Owners", "Owner");
            }

            return BadRequest();
        }

        [HttpGet]
        [Route("~/Owner/EditOwner")]
        public IActionResult EditOwner(string email)
        {
            var owner = _context.Owner.FirstOrDefault(o => o.OwnerEmail == email);
            var ownerToEdit = new OwnerToEdit()
            {
                OwnerEmail = owner.OwnerEmail,
                OwnerName = owner.OwnerName,
                OwnerSurname = owner.OwnerSurname,
                OwnerPhone = owner.OwnerPhone
            };
            return View(ownerToEdit);
        }

        [HttpPost]
        [Route("~/Owner/EditOwner")]
        public async Task<IActionResult> EditOwner(OwnerToEdit ownerToEdit)
        {
            var owner = _context.Owner.FirstOrDefault(o => o.OwnerEmail == ownerToEdit.OwnerEmail);

            if (owner != null)
            {
                owner.OwnerName = ownerToEdit.OwnerName;
                owner.OwnerSurname = ownerToEdit.OwnerSurname;
                owner.OwnerPhone = ownerToEdit.OwnerPhone;

                _context.Owner.Update(owner);
                await _context.SaveChangesAsync();

                return RedirectToAction("Owners", "Owner");
            }

            return BadRequest();
        }

        [HttpGet]
        [Route("~/Owner/AddOwner")]
        public IActionResult AddOwner()
        {
            ViewBag.Cities = new SelectList(_context.City, "CityID", "CityName");
            ViewBag.OwnerTypes = new SelectList(_context.OwnerType, "OwnerTypeID", "OwnerTypeName");
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> AddOwner(OwnerToAdd ownerToAdd)
        {
            if (ModelState.IsValid)
            {
                if (_context.Owner.Select(e => e.OwnerEmail).Contains(ownerToAdd.OwnerEmail))
                {
                    ModelState.AddModelError("", "Owner with such email already exists");
                    return View();
                }

                var owner = new Owner()
                {
                    OwnerEmail = ownerToAdd.OwnerEmail,
                    OwnerName = ownerToAdd.OwnerName,
                    OwnerSurname = ownerToAdd.OwnerSurname,
                    OwnerPhone = ownerToAdd.OwnerPhone,
                    CityID = ownerToAdd.CityID,
                    PaymentPercent = ownerToAdd.PaymentPercent,
                    OwnerTypeID = ownerToAdd.OwnerTypeID,
                    CreateDate = DateTime.Now,
                    UpdateDate = DateTime.Now
                };

                await _context.Owner.AddAsync(owner);
                await _context.SaveChangesAsync();

                return RedirectToAction("Owners", "Owner");
            }

            ViewBag.OwnerTypes = new SelectList(_context.OwnerType, "OwnerTypeID", "OwnerTypeName");
            ViewBag.Cities = new SelectList(_context.City, "CityID", "CityName");
            return View();
        }
    }
}
