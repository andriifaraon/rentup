﻿using DataLayer.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using RentUpWeb.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RentUpWeb.Controllers
{
    [Route("[controller]/[action]")]
    public class SpaceController : Controller
    {
        private readonly ApplicationDbContext _context;
        public SpaceController(ApplicationDbContext context) => _context = context;

        [HttpGet]
        [Route("~/Space/AddSpace")]
        public IActionResult AddSpace()
        {
            var buiildings = _context.Building.Select(b => new BuildingCityStreetNumber()
            {
                BuildingID = b.BuildingID,
                CityStreetNumber = b.City.CityName + ", " + b.BuildingStreet + ", " + b.BuildingNumber
            });

            ViewBag.Buildings = new SelectList(buiildings, "BuildingID", "CityStreetNumber");
            ViewBag.Owners = new SelectList(_context.Owner, "OwnerEmail", "OwnerEmail");
            ViewBag.SpaceTypes = new SelectList(_context.SpaceType, "SpaceTypeID", "SpaceTypeName");
            return View();
        }

        [HttpGet]
        [Route("~/Space/EditSpace")]
        public IActionResult EditSpace(int? id)
        {
            var space = _context.Space.FirstOrDefault(s => s.SpaceID == id);
            var spaceToEdit = new SpaceToEdit()
            {
                SpaceID = space.SpaceID,
                SpaceName = space.SpaceName,
                Price = (int)space.Price,
                RentalDays = space.RentalDays
            };
            return View(spaceToEdit);
        }

        [HttpPost]
        [Route("~/Space/EditSpace")]
        public async Task<IActionResult> EditSpace(SpaceToEdit spaceToEdit)
        {
            var space = _context.Space.FirstOrDefault(s => s.SpaceID == spaceToEdit.SpaceID);

            if(space != null)
            {
                space.SpaceName = spaceToEdit.SpaceName;
                space.RentalDays = spaceToEdit.RentalDays;
                space.Price = spaceToEdit.Price;

                _context.Space.Update(space);
                await _context.SaveChangesAsync();

                return RedirectToAction("Spaces", "Space");
            }

            return BadRequest();
        }

        [HttpPost]
        public async Task<IActionResult> AddSpace(SpaceToAdd spaceToAdd)
        {
            if (ModelState.IsValid)
            {
                var space = new Space()
                {
                    SpaceName = spaceToAdd.SpaceName,
                    FloorNumber = spaceToAdd.FloorNumber,
                    RoomNumber = spaceToAdd.RoomNumber,
                    Square = spaceToAdd.Square,
                    RentalDays = spaceToAdd.RentalDays,
                    Price = spaceToAdd.Price,
                    OwnerEmail = spaceToAdd.OwnerEmail,
                    SpaceTypeID = spaceToAdd.SpaceTypeID,
                    BuildingID = spaceToAdd.BuildingID,
                    CreateDate = DateTime.Now,
                    UpdateDate = DateTime.Now
                };

                await _context.Space.AddAsync(space);
                await _context.SaveChangesAsync();

                return RedirectToAction("Spaces", "Space");
            }

            var buiildings = _context.Building.Select(b => new BuildingCityStreetNumber()
            {
                BuildingID = b.BuildingID,
                CityStreetNumber = b.City.CityName + ", " + b.BuildingStreet + ", " + b.BuildingNumber
            });

            ViewBag.Buildings = new SelectList(buiildings, "BuildingID", "CityStreetNumber");
            ViewBag.Owners = new SelectList(_context.Owner, "OwnerEmail", "OwnerEmail");
            ViewBag.SpaceTypes = new SelectList(_context.SpaceType, "SpaceTypeID", "SpaceTypeName");
            return View();
        }

        [HttpGet]
        [Route("~/Space/Spaces")]
        public IActionResult Spaces()
        {
            return View(_context.Space.Select
                                            (s => new SpaceToList()
                                            {
                                                SpaceID = s.SpaceID,
                                                SpaceName = s.SpaceName,
                                                City = s.Building.City.CityName,
                                                OwnerEmail = s.OwnerEmail,
                                                CanBeDeleted = s.Contracts.Count == 0
                                            }
                                            )
                                            .ToList());
        }

        [Route("~/Space/RemoveSpace")]
        public async Task<IActionResult> RemoveSpace(int id)
        {
            var space = _context.Space.FirstOrDefault(s => s.SpaceID == id);

            if (space != null)
            {
                _context.Space.Remove(space);
                await _context.SaveChangesAsync();

                return RedirectToAction("Spaces", "Space");
            }

            return BadRequest();
        }

        [Route("~/Space/Details/{id}")]
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
                return NotFound();

            var space = await _context.Space.FirstOrDefaultAsync(s => s.SpaceID == id);

            if (space == null)
                return NotFound();

            return View(
                new SpaceToDetails()
                {
                    SpaceName = space.SpaceName,
                    RoomNumber = space.RoomNumber,
                    FloorNumber = space.FloorNumber,
                    RentalDays = space.RentalDays,
                    Price = space.Price,
                    Square = space.Square,
                    SpaceTypeName = space.SpaceType.SpaceTypeName,
                    OwnerEmail = space.OwnerEmail,
                    NameSurname = space.Owner.OwnerName + " " + space.Owner.OwnerSurname,
                    OwnerPhone = space.Owner.OwnerPhone,
                    Address = space.Building.BuildingStreet + ", " + space.Building.BuildingNumber,
                    City = space.Building.City.CityName
                });
        }
    }
}
