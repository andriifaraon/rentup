﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RentUpWeb.Util
{
    public static class SearchStringUtil
    {
        public static bool Match(string searchCriteria, params string [] searchSource )
            => searchSource.Any(s => s.Contains(searchCriteria, StringComparison.OrdinalIgnoreCase));
    }
}
