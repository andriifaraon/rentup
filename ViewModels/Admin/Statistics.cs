﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RentUpWeb.ViewModels
{
    public class Statistics
    {
        public List<Payment> Payments { get; set; }
        public List<ContractToList> Contracts { get; set; }
        public decimal TotalIncome { get; set; }
        public decimal TotalOutcome { get; set; }
        public decimal CompanyBalance { get; set; }
        public decimal Profit { get; set; }
        public int WorkingEmployees { get; set; }
        public int FiredEmployees { get; set; }
        public int RetiredEmployees { get; set; }
        public int ToBeFiredEmployees { get; set; }
        public int SpacesCount { get; set; }
        public int OwnersCount { get; set; }
        public int ClientsCount { get; set; }
        public int CitiesCount { get; set; }
    }
}
