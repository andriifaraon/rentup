﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RentUpWeb.ViewModels
{
    public class BuildingCityStreetNumber
    {
        public int BuildingID { get; set; }
        public string CityStreetNumber { get; set; }
    }
}
