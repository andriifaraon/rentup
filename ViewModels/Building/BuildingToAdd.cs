﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RentUpWeb.ViewModels
{
    public class BuildingToAdd
    {
        [Required]
        [Display(Name = "City")]
        public int CityID { get; set; }

        [Required]
        [Display (Name = "Street")]
        [MinLength(2)]
        [MaxLength(60)]
        public string BuildingStreet { get; set; }

        [Required]
        [Display(Name = "Number")]
        public int BuildingNumber { get; set; }
    }
}
