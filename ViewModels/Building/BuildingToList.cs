﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RentUpWeb.ViewModels
{
    public class BuildingToList
    {
        [Display (Name = "City")]
        public string City { get; set; }

        [Display(Name = "Street")]
        public string BuildingStreet { get; set; }

        [Display(Name = "Number")]
        public int BuildingNumber { get; set; }
    }
}
