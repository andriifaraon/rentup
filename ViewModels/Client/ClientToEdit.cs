﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RentUpWeb.ViewModels
{
    public class ClientToEdit
    {
        public string ClientEmail { get; set; }

        [Required]
        [Display(Name = "Name")]
        [MinLength(2)]
        [MaxLength(50)]
        [RegularExpression("^[A-ZА-Я][a-zа-я]*$")]
        public string ClientName { get; set; }

        [Required]
        [Display(Name = "Surname")]
        [MinLength(2)]
        [MaxLength(50)]
        [RegularExpression("^[A-ZА-Я][a-zа-я]*$")]
        public string ClientSurname { get; set; }

        [Required]
        [Display(Name = "Phone")]
        [Phone]
        public string ClientPhone { get; set; }
    }
}
