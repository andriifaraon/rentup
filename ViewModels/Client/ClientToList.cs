﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RentUpWeb.ViewModels
{
    public class ClientToList
    {
        [Display(Name = "Email")]
        public string ClientEmail { get; set; }

        [Display(Name = "Name")]
        public string ClientNameSurname { get; set; }

        [Display(Name = "Phone")]
        public string ClientPhone { get; set; }

        [Display(Name = "City")]
        public string CityName { get; set; }

        public bool CanBeDeleted { get; set; }
    }
}
