﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RentUpWeb.ViewModels
{
    public class ContractToCreate
    {
        [Required]
        public DateTime EndDate { get; set; }

        [Required]
        public DateTime StartDate { get; set; }

        [Required]
        public string ClientEmail { get; set; }

        [Required]
        public string EmployeeEmail { get; set; }

        [Required]
        public int SpaceID { get; set; }
    }
}
