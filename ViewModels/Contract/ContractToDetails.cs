﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RentUpWeb.ViewModels
{
    public class ContractToDetails
    {
        public int ContractID { get; set; }

        [Display(Name = "Sum of Contract")]
        public decimal ContractSum { get; set; }

        [Display(Name = "Signed Date")]
        public DateTime SignedDate { get; set; }

        [Display(Name = "Period")]
        public string Period { get; set; }

        [Display(Name = "Sum of Tax")]
        public decimal TaxSum { get; set; }

        [Display(Name = "State of Contract")]
        public string ContractState { get; set; }

        [Display(Name = "Owner")]
        public string OwnerEmail { get; set; }

        [Display(Name = "Client")]
        public string ClientEmail { get; set; }

        [Display(Name = "Space Name")]
        public string SpaceName { get; set; }

        [Display(Name = "Payment from Owner")]
        public decimal OwnerPayment { get; set; }

        [Display(Name = "Type of Space")]
        public string SpaceTypeName { get; set; }
    }
}
