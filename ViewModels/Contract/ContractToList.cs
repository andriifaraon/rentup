﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RentUpWeb.ViewModels
{
    public class ContractToList
    {
        [Display(Name = "#Id")]
        public int ContractID { get; set; }

        [Display(Name = "Owner")]
        public string OwnerEmail { get; set; }

        [Display(Name = "Client")]
        public string ClientEmail { get; set; }
    }
}
