﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RentUpWeb.ViewModels
{
    public class EmployeeRate
    {
        [Display(Name = "Email")]
        public string EmployeeEmail { get; set; }

        [Display(Name = "Name")]
        public string EmployeeName { get; set; }

        [Display(Name = "Surname")]
        public string EmployeeSurname { get; set; }

        [Display(Name = "Position")]
        public string EmployeeTypeName { get; set; }

        [Display(Name = "Department Email")]
        public string DepartmentEmail { get; set; }

        [Display(Name = "Contract Percent")]
        public decimal ContractPercent { get; set; }

        [Display(Name = "Account Sum")]
        public decimal ContractSum { get; set; }

        [Display(Name = "Contracts Number")]
        public int ContractNumber { get; set; }
    }
}
