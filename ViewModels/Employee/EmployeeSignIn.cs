﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RentUpWeb.ViewModels
{
    public class EmployeeSignIn
    {
        [Required(ErrorMessage = "Email is required")]
        [Display(Name = "Email")]
        public string EmployeeEmail { get; set; }
        [Required(ErrorMessage = "Password is required")]
        [Display(Name = "Password")]
        public string EmployeePassword { get; set; }
    }
}
