﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RentUpWeb.ViewModels
{
    public class EmployeeToHire
    {
        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        public string EmployeeEmail { get; set; }

        [Required]
        [Display(Name = "Password")]
        [MinLength(4)]
        public string Password { get; set; }

        [Required]
        [Display(Name = "Name")]
        [MinLength(2)]
        [MaxLength(50)]
        [RegularExpression("^[A-ZА-Я][a-zа-я]*$")]
        public string EmployeeName { get; set; }

        [Required]
        [Display(Name = "Surname")]
        [MinLength(2)]
        [MaxLength(50)]
        [RegularExpression("^[A-ZА-Я][a-zа-я]*$")]
        public string EmployeeSurname { get; set; }

        [Required]
        [Display(Name = "Percent")]
        public decimal EmployeePercent { get; set; }

    }
}
