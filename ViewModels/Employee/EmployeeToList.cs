﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RentUpWeb.ViewModels
{
    public class EmployeeToList
    {
        [Display(Name = "Email")]
        public string EmployeeEmail { get; set; }

        [Display(Name = "Name")]
        public string EmployeeNameSurname { get; set; }

        [Display(Name = "Position")]
        public string EmployeePositionName { get; set; }

        [Display(Name = "State")]
        public string EmployeeStateName { get; set; }

        public int EmployeeStateID { get; set; }
        public int EmployeeTypeID { get; set; }
    }
}
