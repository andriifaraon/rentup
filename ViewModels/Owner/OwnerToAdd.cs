﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RentUpWeb.ViewModels
{
    public class OwnerToAdd
    {
        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        public string OwnerEmail { get; set; }

        [Required]
        [Display(Name = "Name")]
        [MinLength(2)]
        [MaxLength(50)]
        [RegularExpression("^[A-ZА-Я][a-zа-я]*$")]
        public string OwnerName { get; set; }

        [Required]
        [Display(Name = "Surname")]
        [MinLength(2)]
        [MaxLength(50)]
        [RegularExpression("^[A-ZА-Я][a-zа-я]*$")]
        public string OwnerSurname { get; set; }

        [Required]
        [Display(Name = "Phone")]
        [Phone]
        public string OwnerPhone { get; set; }

        [Required]
        public int CityID { get; set; }

        [Required]
        public int OwnerTypeID { get; set; }

        [Required]
        [Display(Name = "Percent")]
        public decimal PaymentPercent { get; set; }
    }
}
