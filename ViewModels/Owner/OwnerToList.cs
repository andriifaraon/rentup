﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RentUpWeb.ViewModels
{
    public class OwnerToList
    {
        [Display (Name = "Email")]
        public string OwnerEmail { get; set; }

        [Display(Name = "Name")]
        public string OwnerNameSurname { get; set; }

        [Display(Name = "Phone")]
        public string OwnerPhone { get; set; }

        [Display(Name = "City")]
        public string City { get; set; }

        [Display(Name = "Type")]
        public string OwnerType { get; set; }

        [Display(Name = "Percent")]
        public decimal PaymentPercent { get; set; }

        public bool CanBeDeleted { get; set; }
    }
}
