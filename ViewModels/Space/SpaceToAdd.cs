﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RentUpWeb.ViewModels
{
    public class SpaceToAdd
    {
        [Required]
        [Display (Name = "Space Name")]
        [MinLength(3)]
        [MaxLength(50)]
        public string SpaceName { get; set; }

        [Display(Name = "Floor")]
        public int? FloorNumber { get; set; }

        [Display(Name = "Room")]
        public int? RoomNumber { get; set; }

        [Display(Name = "Square")]
        public decimal? Square { get; set; }

        [Required]
        [Display(Name = "Rental Days")]
        public int RentalDays { get; set; }

        [Required]
        [Display(Name = "Price")]
        public int Price { get; set; }

        [Required]
        [Display(Name = "Owner")]
        public string OwnerEmail { get; set; }

        [Required]
        [Display(Name = "Type")]
        public int SpaceTypeID { get; set; }

        [Required]
        [Display(Name = "Building")]
        public int BuildingID { get; set; }
    }
}
