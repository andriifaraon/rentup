﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RentUpWeb.ViewModels
{
    public class SpaceToDetails
    {
        #region SpaceInfo
        [Display (Name = "Space Name")]
        public string SpaceName { get; set; }

        [Display(Name = "Floor")]
        public int? FloorNumber { get; set; }

        [Display(Name = "Room")]
        public int? RoomNumber { get; set; }

        [Display(Name = "Square")]
        public decimal? Square { get; set; }

        [Display(Name = "Rental Days")]
        public int RentalDays { get; set; }

        [Display(Name = "Price")]
        public decimal Price { get; set; }

        [Display(Name = "Type")]
        public string SpaceTypeName { get; set; }
        #endregion


        #region Owner 
        [Display(Name = "Email")]
        public string OwnerEmail { get; set; }

        [Display(Name = "Name")]
        public string NameSurname { get; set; }

        [Display(Name = "Phone")]
        public string OwnerPhone { get; set; }
        #endregion


        #region Building
        [Display(Name = "Address")]
        public string Address { get; set; }

        [Display(Name = "City")]
        public string City { get; set; }
        #endregion
    }
}
