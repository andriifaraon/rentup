﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RentUpWeb.ViewModels
{
    public class SpaceToEdit
    {
        public int SpaceID { get; set; }

        [Required]
        [Display(Name = "Space Name")]
        [MinLength(3)]
        [MaxLength(50)]
        public string SpaceName { get; set; }

        [Required]
        [Display(Name = "Price")]
        public int Price { get; set; }

        [Required]
        [Display(Name = "Rental Days")]
        public int RentalDays { get; set; }
    }
}
