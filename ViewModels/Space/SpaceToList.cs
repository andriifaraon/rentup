﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RentUpWeb.ViewModels
{
    public class SpaceToList
    {
        public int SpaceID { get; set; }

        [Display(Name = "Space Name")]
        public string SpaceName { get; set; }

        [Display(Name = "City")]
        public string City { get; set; }

        [Display(Name = "Email")]
        public string OwnerEmail { get; set; }

        public bool CanBeDeleted { get; set; }
    }
}
